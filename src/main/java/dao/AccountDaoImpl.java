package dao;

import db.InMemoryDB;
import model.Account;
import model.Persistable;
import validators.AccountValidator;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class AccountDaoImpl implements AccountDao {

    private final InMemoryDB inMemoryDB;
    private final ReentrantLock lock;

    public AccountDaoImpl(InMemoryDB inMemoryDB) {
        if (inMemoryDB == null) {
            throw new RuntimeException("DB can't be null");
        }
        this.inMemoryDB = inMemoryDB;
        this.lock = new ReentrantLock();
    }

    @Override
    public Account create(Account account) {
        AccountValidator.validateAccount(account);
        inMemoryDB.save(account);
        return account;
    }

    @Override
    public Account findById(Long id) throws AccountNotFoundException {
        List<? extends Persistable> read = inMemoryDB.read(Collections.singletonList(id));
        if (read.isEmpty() || !(read.get(0) instanceof Account)) {
            throw new AccountNotFoundException("Account with id " + id + " doesn't exist");
        }
        return (Account) read.get(0);
    }

    @Override
    public void transferMoney(Account from, Account to, BigDecimal amount) {
        AccountValidator.validateTransferRequest(from, to, amount);
        try {
            lock.lock();
            BigDecimal fromNewBalance = from.getBalance().subtract(amount);
            if (fromNewBalance.compareTo(BigDecimal.ZERO) < 0) {
                throw new NotEnoughMoneyException("account " + from.getId()
                        + " doesn't have enough money to transfer " + amount + "$ to account " + to.getId());
            }
            BigDecimal toNewBalance = to.getBalance().add(amount);
            from.setBalance(fromNewBalance);
            to.setBalance(toNewBalance);
            inMemoryDB.update(from);
            inMemoryDB.update(to);
        } finally {
            lock.unlock();
        }
    }
}
