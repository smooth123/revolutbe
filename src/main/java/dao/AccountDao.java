package dao;

import model.Account;

import java.math.BigDecimal;

public interface AccountDao {
    Account create(Account account);

    Account findById(Long id) throws AccountNotFoundException;

    /**
     * @brief Synchronization is needed in order to avoid inconsistent state when transferring money from multiple threads.
     * Although underlying data-storage is synchronized we still require a top level synchronization to make the whole
     * transfer atomic.
     *
     * In real world we would use RDBMS (e.g. MySql) and leverage ACID properties of transactions to support concurrent transfers
     *
     * One possible solution to support concurrent transfers is to create a lock object for each pair of accounts, e.g. we need
     * N * (N - 1) / 2 locks (N = number of accounts) and then when transferring money from A to B we could synchronize on AB lock.
     *
     * Since only one writer is allowed in the inMemoryDB this approach won't "parallelize" much.
     *
     * @param amount should be positive ( > 0 )
     */
    void transferMoney(Account from, Account to, BigDecimal amount);
}
