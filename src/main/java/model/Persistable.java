package model;

import java.io.Serializable;

public interface Persistable extends Serializable {
    void setId(long id);
    long getId();
}
