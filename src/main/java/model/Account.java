package model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class Account implements Persistable {
    private long id;
    private String number;
    private BigDecimal balance;
}

