import dao.AccountDao;
import dao.AccountDaoImpl;
import dao.AccountNotFoundException;
import dao.NotEnoughMoneyException;
import db.InMemoryDB;
import io.javalin.ExceptionHandler;
import io.javalin.Javalin;
import model.Account;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rest.InvalidRequest;
import rest.MoneyTransferController;

import java.math.BigDecimal;

public class App {

    public static final int PORT = 9998;

    private static Javalin app;

    private static final Logger LOG = LoggerFactory.getLogger(App.class);

    static AccountDao accountDao;


    private static void fillDB(AccountDao accountDao) {
        accountDao.create(Account.builder()
                .number("011")
                .balance(new BigDecimal("1000"))
                .build());
        accountDao.create(Account.builder()
                .number("022")
                .balance(new BigDecimal("1000"))
                .build());
        accountDao.create(Account.builder()
                .number("033")
                .balance(new BigDecimal("1000"))
                .build());
        accountDao.create(Account.builder()
                .number("044")
                .balance(new BigDecimal("1000"))
                .build());
    }

    public static void main(String[] args) {
        start();
    }

    public static void start() {
        app = Javalin.create()
                .start(PORT);
        accountDao = new AccountDaoImpl(new InMemoryDB());
        fillDB(accountDao);
        app.get("/", ctx -> ctx.result("test test"));
        app.post(MoneyTransferController.PATH, new MoneyTransferController(accountDao));
        ExceptionHandler<Exception> handler = (e, ctx) -> {
            LOG.error(e.getMessage());
            ctx.status(HttpStatus.BAD_REQUEST_400);
            ctx.result(e.toString());
        };
        app.exception(IllegalArgumentException.class, handler);
        app.exception(NotEnoughMoneyException.class, handler);
        app.exception(AccountNotFoundException.class, handler);
        app.exception(InvalidRequest.class, handler);
    }

    public static void stop() {
        app.stop();
    }
}
