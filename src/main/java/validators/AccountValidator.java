package validators;

import model.Account;

import java.math.BigDecimal;

public class AccountValidator {

    public static void validateAccount(Account account) {
        if (account == null) {
            throw new IllegalArgumentException("Account can't be null");
        }
        if (account.getBalance().compareTo(BigDecimal.ZERO) < 0) {
            throw new InvalidAccountException("Account can't have negative balance");
        }
        if (account.getNumber() == null || account.getNumber().isEmpty()) {
            throw new InvalidAccountException("Accaount number can't be empty");
        }
    }

    public static void validateTransferRequest(Account from, Account to, BigDecimal amount) {
        if (from == null || to == null) {
            throw new IllegalArgumentException("from and to accounts should be non null");
        }
        if (from.equals(to)) {
            throw new IllegalArgumentException("from and to accounts should be different");
        }
        if (amount == null || (amount.compareTo(BigDecimal.ZERO) <= 0)) {
            throw new IllegalArgumentException("amount should be > 0");
        }
    }

}
