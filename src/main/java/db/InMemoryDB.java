package db;

import model.Persistable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

public class InMemoryDB {

    /**
     * @brief Simple in memory storage emulation. Supports only one concurrent writer and many concurrent readers.
     *        Stores byte arrays. Creates copies on save operations and returns copies on read operations.
     */

    private long idGenerator;

    private final Map<Long, byte[]> table;
    private final ReadWriteLock readWriteLock;
    private final Lock readLock;
    private final Lock writeLock;

    public InMemoryDB() {
        this.idGenerator = 0L;
        this.table = new HashMap<>();
        this.readWriteLock = new ReentrantReadWriteLock();
        this.readLock = readWriteLock.readLock();
        this.writeLock = readWriteLock.writeLock();
    }

    public <T extends Persistable> void save(T object) {
        validateObject(object);
        writeLock.lock();
        try {
            long id = ++idGenerator;
            object.setId(id);
            byte[] clonedObject = toByteArray(object).clone();
            table.put(id, clonedObject);
        } finally {
            writeLock.unlock();
        }
    }

    public void delete(long id) {
        validateId(id);
        writeLock.lock();
        try {
            if (!table.containsKey(id)) {
                throw new DBException("Key error: no object with id " + id);
            }
            table.remove(id);
        } finally {
            writeLock.unlock();
        }
    }

    public <T extends Persistable> void update(T object) {
        validateObject(object);
        writeLock.lock();
        try {
            if (!table.containsKey(object.getId())) {
                throw new DBException("Key error: no object with id " + object.getId());
            }
            byte[] clonedObject = toByteArray(object).clone();
            table.put(object.getId(), clonedObject);
        } finally {
            writeLock.unlock();
        }
    }

    public List<? extends Persistable> read(List<Long> ids) {
        readLock.lock();
        try {
            return ids.stream()
                    .filter(this::validateId)
                    .filter(table::containsKey)
                    .map(table::get)
                    .map(byte[]::clone)
                    .map(this::fromByteArray)
                    .map(x -> (Persistable) x)
                    .collect(Collectors.toList());

        } finally {
            readLock.unlock();
        }
    }

    private boolean validateId(long id) {
        if (id <= 0) {
            throw new DBException("Persistable should be positive");
        }
        return true;
    }

    private boolean validateObject(Object object) {
        if (object == null) {
            throw new DBException("Can't persist null object");
        }
        return true;
    }

    private byte[] toByteArray(Persistable persistable) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(bos)) {
            oos.writeObject(persistable);
            oos.flush();
            return bos.toByteArray();
        } catch (IOException e) {
            throw new DBException("Internal db error has occurred");
        }
    }

    private Object fromByteArray(byte [] array) {
        try (ByteArrayInputStream bais = new ByteArrayInputStream(array);
             ObjectInputStream ois = new ObjectInputStream(bais)) {
            return ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new DBException("Internal db error has occurred");
        }
    }
}
