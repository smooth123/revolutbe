package rest;

import dao.AccountDao;
import io.javalin.Context;
import io.javalin.Handler;
import model.Account;
import org.eclipse.jetty.http.HttpStatus;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;

public class MoneyTransferController implements Handler {

    private final AccountDao accountDao;

    public static final String FROM_ACC_PARAM = "from";
    public static final String TO_ACC_PARAM = "to";
    public static final String SUM_PARAM = "sum";
    public static final String PATH = "/transfer/:" + SUM_PARAM;

    public static final String errMsg = "Invalid parameters, expected is: /transfer/<BigDecimal>&from=<long>&to=<long>";

    public MoneyTransferController(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public void handle(@NotNull Context ctx) {
        String sumStr = ctx.pathParam(SUM_PARAM);
        String fromStr = ctx.queryParam(FROM_ACC_PARAM);
        String toStr = ctx.queryParam(TO_ACC_PARAM);
        if (fromStr == null || fromStr.isEmpty() || toStr == null || toStr.isEmpty()) {
            throw new InvalidRequest(errMsg);
        }
        BigDecimal sum;
        try {
            sum = new BigDecimal(sumStr);
        } catch (NumberFormatException e) {
            throw new InvalidRequest(errMsg);
        }
        Account from = accountDao.findById(Long.parseLong(fromStr));
        Account to = accountDao.findById(Long.parseLong(toStr));
        accountDao.transferMoney(from, to, sum);
        ctx.result("Successfully transferred " + sum + "$ from account: " + from + ", to account: " + to);
        ctx.status(HttpStatus.ACCEPTED_202);
    }
}
