package db;

import model.Account;
import model.Persistable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class InMemoryDBTest {

    private InMemoryDB inMemoryDB;

    @Before
    public void setup() {
        inMemoryDB = new InMemoryDB();
    }

    private Account buildAccount() {
        Random random = new Random();
        return Account.builder()
                .number(Integer.toString(random.nextInt(Integer.MAX_VALUE)))
                .balance(BigDecimal.valueOf(random.nextInt(Integer.MAX_VALUE)))
                .build();
    }

    @Test
    public void testSave() {
        Account account1 = buildAccount();
        inMemoryDB.save(account1);
        Account account2 = buildAccount();
        inMemoryDB.save(account2);

        Assert.assertTrue(account1.getId() > 0);
        Assert.assertTrue(account2.getId() > 0);
        Assert.assertNotEquals(account1.getId(), account2.getId());
    }

    @Test
    public void testUpdate() {
        Account account1 = buildAccount();
        inMemoryDB.save(account1);

        List<? extends Persistable> read = inMemoryDB.read(Collections.singletonList(account1.getId()));

        Assert.assertEquals(1, read.size());
        Assert.assertEquals(read.get(0), account1);
        Assert.assertNotSame(read.get(0), account1);

        account1.setBalance(BigDecimal.TEN);

        inMemoryDB.update(account1);

        List<? extends Persistable> readNew = inMemoryDB.read(Collections.singletonList(account1.getId()));

        Assert.assertEquals(1, readNew.size());
        Assert.assertEquals(readNew.get(0), account1);
        Assert.assertNotSame(readNew.get(0), account1);
    }

    @Test
    public void testDelete() {
        Account account1 = buildAccount();
        inMemoryDB.save(account1);

        List<? extends Persistable> read = inMemoryDB.read(Collections.singletonList(account1.getId()));

        Assert.assertEquals(1, read.size());
        Assert.assertEquals(read.get(0), account1);
        Assert.assertNotSame(read.get(0), account1);

        account1.setBalance(BigDecimal.TEN);

        inMemoryDB.delete(account1.getId());

        List<? extends Persistable> readNew = inMemoryDB.read(Collections.singletonList(account1.getId()));

        Assert.assertTrue(readNew.isEmpty());
    }

}
