import org.eclipse.jetty.http.HttpStatus;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import rest.MoneyTransferController;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class AppIntegrationTest {

    /**
     * App creates 4 accounts during bootstrap with ids 1,2,3,4 (each has 1000$). These accounts are used in this test
     */

    private HttpClient httpClient;

    @BeforeClass
    public static void setupStatic() {
        App.start();
    }

    @AfterClass
    public static void destroyStatic() {
        App.stop();
    }

    @Before
    public void setup() {
        httpClient = HttpClient.newHttpClient();
    }

    @Test
    public void transferToNonExistingAccountTest() throws IOException, InterruptedException {
        //when
        HttpResponse<String> response = httpClient.send(makeReq("100", "1", "999"), HttpResponse.BodyHandlers.ofString());
        //then
        Assert.assertEquals(response.statusCode(), HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void transferNegativeAmountTest() throws IOException, InterruptedException {
        //when
        HttpResponse<String> response = httpClient.send(makeReq("-100", "1", "2"), HttpResponse.BodyHandlers.ofString());
        //then
        Assert.assertEquals(response.statusCode(), HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void transferFromToSameAccountTest() throws IOException, InterruptedException {
        //when
        HttpResponse<String> response = httpClient.send(makeReq("100", "1", "1"), HttpResponse.BodyHandlers.ofString());
        //then
        Assert.assertEquals(response.statusCode(), HttpStatus.BAD_REQUEST_400);
    }

    @Test
    public void transferMoneyOK() throws IOException, InterruptedException {
        //when
        HttpResponse<String> response = httpClient.send(makeReq("100", "1", "2"), HttpResponse.BodyHandlers.ofString());
        //then
        Assert.assertEquals(response.statusCode(), HttpStatus.ACCEPTED_202);
        Assert.assertEquals(App.accountDao.findById(1L).getBalance(), new BigDecimal("900"));
        Assert.assertEquals(App.accountDao.findById(2L).getBalance(), new BigDecimal("1100"));
    }

    private HttpRequest makeReq(String sum, String from, String to) {
        URI uri = URI
                .create("http://localhost:" + App.PORT + "/transfer/" + sum + "?"
                        + MoneyTransferController.FROM_ACC_PARAM + "=" + from
                        + "&"
                        + MoneyTransferController.TO_ACC_PARAM + "=" + to);
        return HttpRequest.newBuilder(uri).POST(HttpRequest.BodyPublishers.noBody()).build();
    }
}
