package rest;

import dao.AccountDao;
import io.javalin.Context;
import io.javalin.Javalin;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

public class MoneyTransferControllerTest {

    private AccountDao accountDao;
    private MoneyTransferController controller;
    private int httpStatus;

    @Before
    public void setup() {
        accountDao = spy(AccountDao.class);
        controller = new MoneyTransferController(accountDao);
    }

    @Test(expected = InvalidRequest.class)
    public void testInvalidSum() {
        //when
        Context req = makeRequest("asd", "1", "2");
        controller.handle(req);
    }

    @Test(expected = InvalidRequest.class)
    public void testEmptyFromAccount() {
        //when
        Context req = makeRequest("100", "", "1");
        controller.handle(req);
    }

    @Test(expected = InvalidRequest.class)
    public void testEmptyToAccount() {
        //when
        Context req = makeRequest("100", "1", "");
        controller.handle(req);
    }

    @Test(expected = InvalidRequest.class)
    public void testEmptyFromAndToAccount() {
        //when
        Context req = makeRequest("100", "", "");
        controller.handle(req);
    }

    @Test
    public void testTransferOK() {
        //given
        doNothing().when(accountDao).transferMoney(any(), any(), any());
        //when
        Context req = makeRequest("100", "1", "2");
        controller.handle(req);
        //then
        Assert.assertEquals(httpStatus, HttpStatus.ACCEPTED_202);
        Mockito.verify(accountDao, times(1)).transferMoney(any(), any(), any());
    }

    private Context makeRequest(String sum, String from, String to) {
        HttpServletRequest httpReq = mock(HttpServletRequest.class);
        when(httpReq.getQueryString())
                .thenReturn(MoneyTransferController
                        .FROM_ACC_PARAM + "=" + from + "&" + MoneyTransferController.TO_ACC_PARAM + "=" + to);

        HttpServletResponse httpResp = mock(HttpServletResponse.class);
        doAnswer(invocation -> {
            httpStatus = invocation.getArgument(0);
            return null;
        }).when(httpResp).setStatus(anyInt());

        Context context = new Context(httpReq, httpResp, mock(Javalin.class));
        try {
            setField(context, "pathParamMap", Collections.singletonMap(MoneyTransferController.SUM_PARAM, sum));
            setField(context, "matchedPath", MoneyTransferController.PATH);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return context;
    }

    private void setField(Context context, String fieldName, Object fieldValue)
            throws NoSuchFieldException, IllegalAccessException {
        Field pathParamMap = Context.class.getDeclaredField(fieldName);
        pathParamMap.setAccessible(true);
        pathParamMap.set(context, fieldValue);
    }
}
