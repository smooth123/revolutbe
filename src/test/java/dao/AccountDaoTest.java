package dao;

import db.InMemoryDB;
import model.Account;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

public class AccountDaoTest {

    private AccountDao accountDao;
    private InMemoryDB inMemoryDB;

    private Account buildAccount(BigDecimal val) {
        Random random = new Random();
        return Account.builder()
                .number(Integer.toString(random.nextInt(Integer.MAX_VALUE)))
                .balance(val)
                .build();
    }

    @Before
    public void setup() {
        inMemoryDB = new InMemoryDB();
        accountDao = new AccountDaoImpl(inMemoryDB);
    }

    @Test(expected = AccountNotFoundException.class)
    public void tesTFindByIdNonExistingAccount() {
        // when
        accountDao.findById(1L);
    }

    @Test
    public void testFindByIdExistingAccount() {
        // given
        Account given = buildAccount(BigDecimal.ZERO);

        // when
        accountDao.create(given);

        // then
        Assert.assertEquals(given, accountDao.findById(given.getId()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTransferMoneyBetweenSameAccount() {
        //given
        Account given = buildAccount(BigDecimal.ZERO);

        //when
        accountDao.transferMoney(given, given, BigDecimal.ONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTransferMoneyNegativeAmount() {
        //given
        Account accountA = buildAccount(BigDecimal.ONE);
        Account accountB = buildAccount(BigDecimal.ZERO);

        //when
        accountDao.transferMoney(accountA, accountB, BigDecimal.valueOf(-10L));
    }

    @Test(expected = NotEnoughMoneyException.class)
    public void testTransferMoneyNotEnoughMoney() {
        //given
        Account accountA = buildAccount(BigDecimal.ONE);
        Account accountB = buildAccount(BigDecimal.ZERO);

        //when
        accountDao.transferMoney(accountA, accountB, BigDecimal.TEN);
    }

    @Test
    public void testTransferMoneyOK() {
        //given
        Account accountA = buildAccount(BigDecimal.TEN);
        Account accountB = buildAccount(BigDecimal.ZERO);
        accountDao.create(accountA);
        accountDao.create(accountB);

        // when
        accountDao.transferMoney(accountA, accountB, BigDecimal.valueOf(5L));

        // then
        Assert.assertEquals(accountDao.findById(accountA.getId()).getBalance(), BigDecimal.valueOf(5L));
        Assert.assertEquals(accountDao.findById(accountB.getId()).getBalance(), BigDecimal.valueOf(5L));
    }

    private LinkedBlockingQueue<Object[]> initTransferRequestQueue(Account accountA, Account accountB) {
        // 1 object of the queue -> [0] from account, [1] to account, [2] amount
        LinkedBlockingQueue<Object[]> queue = new LinkedBlockingQueue<>();

        // from A to B 0.5 amount
        // from A to B 2.5 amount
        // from A to B 0.5 amount
        // from A to B 0.4 amount

        // from B to A 0.5 amount
        // from B to A 1 amount
        // from B to A 1.5 amount
        // ----------------------
        // transferred from A to B total: 0.5 + 2.5 + 0.5 + 0.4 = 3.9
        // transferred from B to A total: 0.5 + 1 + 1.5 = 3
        // **final A's balance 10 - 3.9 + 3 = 9.1
        // **final B's balance 10 - 3 + 3.9 = 10.9

        queue.add(new Object[]{accountA, accountB, new BigDecimal("0.5")});
        queue.add(new Object[]{accountA, accountB, new BigDecimal("2.5")});
        queue.add(new Object[]{accountA, accountB, new BigDecimal("0.5")});
        queue.add(new Object[]{accountA, accountB, new BigDecimal("0.4")});

        queue.add(new Object[]{accountB, accountA, new BigDecimal("0.5")});
        queue.add(new Object[]{accountB, accountA, new BigDecimal("1")});
        queue.add(new Object[]{accountB, accountA, new BigDecimal("1.5")});

        return queue;
    }

    @Test(timeout = 10000)
    public void testTransferMoneyConcurrentlyOK() {
        // given
        Account accountA = buildAccount(BigDecimal.TEN);
        Account accountB = buildAccount(BigDecimal.TEN);

        accountDao.create(accountA);
        accountDao.create(accountB);

        //when
        LinkedBlockingQueue<Object[]> queue = initTransferRequestQueue(accountA, accountB);
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        List<Runnable> tasks = new ArrayList<>();
        // create 5 tasks
        for (int j = 0; j < 5; j++) {
            tasks.add(() -> {
                while (!queue.isEmpty()) {
                    try {
                        Object[] poll = queue.poll(3000, TimeUnit.MILLISECONDS);
                        if (poll == null) {
                            continue;
                        }
                        accountDao.transferMoney((Account) poll[0], (Account) poll[1], (BigDecimal) poll[2]);
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }
        List<Future<?>> futures = tasks.stream().map(executorService::submit).collect(Collectors.toList());
        for (Future<?> f : futures) {
            try {
                f.get(5000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                executorService.shutdownNow();
                throw new RuntimeException(e.getCause());
            }
        }
        executorService.shutdown();
        // then
        Assert.assertEquals(accountDao.findById(accountA.getId()).getBalance(), new BigDecimal("9.1"));
        Assert.assertEquals(accountDao.findById(accountB.getId()).getBalance(), new BigDecimal("10.9"));
    }

    @Test(timeout = 100_000)
    public void testTransferMoneyConcurrentlyOK_10Times() {
        setup();
        testTransferMoneyConcurrentlyOK();
    }
}
