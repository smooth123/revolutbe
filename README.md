### Revolut Backend Test

Simple HTTP REST application for transferring money between accounts

####How to run?
1. `./gradlew clean build`
2. `cd build/distributions`
3. `unzip RevolutBE-1.0-SNAPSHOT.zip`
4. `cd RevolutBE-1.0-SNAPSHOT/bin/`
5. `./RevolutBE`

###How to use?
* App creates 4 accounts during bootstrap with ids 1,2,3,4 (each has 1000$).
This logic is located inside `App.java` class in method `fillDB`
* Money transfer URI looks like this: `http://localhost:9998/transfer/<BigDecimal>&from=<long>&to=<long>`.
* If transfer request was valid, then the response code is `202 ACCEPTED`, otherwise `400 BAD REQUEST`
* Sample post request via curl: `curl -i -X POST http://localhost:9998/transfer/100\?from\=1\&to\=2`

###3rd party components used
* [Javalin](https://javalin.io/) - A simple web framework
for Java and Kotlin
*  [Lombock](https://projectlombok.org/) -  bytecode generation library to avoid of writing of the boilerplate code
* [Mockito](https://site.mockito.org) - mocking framework
* [JUnit](https://junit.org/junit4/) - a simple framework to write repeatable tests
* [Jackson](https://github.com/FasterXML/jackson-core) - a high-performance JSON processor for Java
